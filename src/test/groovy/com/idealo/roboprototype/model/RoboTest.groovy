package com.idealo.roboprototype.model

import com.idealo.roboprototype.excpetions.GenericException
import spock.lang.Specification

class RoboTest extends Specification {


    void setup() {

    }

    def "MoveRoboOnInstruction when direction is EAST"() {
        given:
        def robo=new Robo();
        def instruction=new Instruction();
        robo.setHorizontalPosition(1)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.EAST);
        instruction.setRoboInstruction(Instruction.RoboInstruction.FORWARD);
        instruction.setFirstInstruction(3)

        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo,instruction)
        then:
        instruction1.direction.equals(Robo.Direction.EAST)
        instruction1.getHorizontalPosition()==4
        instruction1.getVerticalPosition()==3
    }


    def "MoveRoboOnInstruction when direction is West"() {
        given:
        def robo=new Robo();
        def instruction=new Instruction();
        robo.setHorizontalPosition(1)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.WEST);
        instruction.setRoboInstruction(Instruction.RoboInstruction.FORWARD);
        instruction.setFirstInstruction(3)

        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo,instruction)
        then:
        thrown(GenericException)
    }

    def "MoveRoboOnInstruction when direction is West when coordinates is 3 ,3 "() {
        given:
        def robo=new Robo();
        def instruction=new Instruction();
        robo.setHorizontalPosition(3)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.WEST);
        instruction.setRoboInstruction(Instruction.RoboInstruction.FORWARD);
        instruction.setFirstInstruction(2)

        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo,instruction)
        then:
        instruction1.direction.equals(Robo.Direction.WEST)
        instruction1.getHorizontalPosition()==1
        instruction1.getVerticalPosition()==3
    }

    def "MoveRoboOnInstruction when direction is North when coordinates is 3 ,3 "() {
        given:
        def robo=new Robo();
        def instruction=new Instruction();
        robo.setHorizontalPosition(3)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.NORTH);
        instruction.setRoboInstruction(Instruction.RoboInstruction.FORWARD);
        instruction.setFirstInstruction(2)

        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo,instruction)
        then:
        instruction1.direction.equals(Robo.Direction.NORTH)
        instruction1.getHorizontalPosition()==3
        instruction1.getVerticalPosition()==1
    }

    def "MoveRoboOnInstruction when direction is South when coordinates is 3 ,3 "() {
        given:
        def robo=new Robo();
        def instruction=new Instruction();
        robo.setHorizontalPosition(3)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.SOUTH);
        instruction.setRoboInstruction(Instruction.RoboInstruction.FORWARD);
        instruction.setFirstInstruction(2)

        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo,instruction)
        then:
        instruction1.direction.equals(Robo.Direction.SOUTH)
        instruction1.getHorizontalPosition()==3
        instruction1.getVerticalPosition()==5
    }

    def "MoveRoboOnInstruction when direction is South And command is Trunarund"() {
        given:
        def robo = new Robo();
        def instruction = new Instruction();
        robo.setHorizontalPosition(3)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.SOUTH);
        instruction.setRoboInstruction(Instruction.RoboInstruction.TURNAROUND);


        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo, instruction)
        then:
        instruction1.direction.equals(Robo.Direction.NORTH)
    }

    def "MoveRoboOnInstruction when Command is Set positino"() {
        given:
        def robo = new Robo();
        def instruction = new Instruction();
        robo.setHorizontalPosition(3)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.SOUTH);
        instruction.setRoboInstruction(Instruction.RoboInstruction.POSITION);
        instruction.setFirstInstruction(6)
        instruction.setSecondInstruction(4)


        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo, instruction)
        then:
        thrown(GenericException)
    }

    def "MoveRoboOnInstruction when Command is Set position with valid coordinates"() {
        given:
        def robo = new Robo();
        def instruction = new Instruction();
        robo.setHorizontalPosition(3)
        robo.setVerticalPosition(3)
        robo.setDirection(Robo.Direction.SOUTH);
        instruction.setRoboInstruction(Instruction.RoboInstruction.POSITION);
        instruction.setFirstInstruction(5)
        instruction.setSecondInstruction(5)
        instruction.setDirection(Robo.Direction.SOUTH)


        when:
        def instruction1 = Robo.moveRoboOnInstruction(robo, instruction)
        then:
        instruction1.getDirection().equals(Robo.Direction.SOUTH)
    }

    def "checkIfRoboCanMoveForwardOnCornerCases when robo on corner"(){
        given:
        def roboCasted=new Robo();
        roboCasted.setHorizontalPosition(1)
        roboCasted.setVerticalPosition(3)
        roboCasted.setDirection(Robo.Direction.EAST);
        when:
        def res=Robo.checkIfRoboCanMoveForwardOnCornerCases(roboCasted)

        then:
        (res)

    }
}
