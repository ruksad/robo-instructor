package com.idealo.roboprototype.model

import com.idealo.roboprototype.excpetions.GenericException
import spock.lang.Specification

class InstructionTest extends Specification {
    def instruction
    void setup() {
    }

    def "IsValidInstruction when instructions is RIGHT"() {
        given:
        def instruction=new Instruction()
        instruction.setRoboInstruction(Instruction.RoboInstruction.RIGHT)
        instruction.setDirection(Robo.Direction.EAST)
        when:
         def res=   Instruction.isValidInstruction(instruction)
        then:
        !res
    }

    def "IsValidInstruction when instructions is WAIT"() {
        given:
        def instruction=new Instruction()
        instruction.setRoboInstruction(Instruction.RoboInstruction.WAIT)
        instruction.setDirection(Robo.Direction.EAST)
        when:
        def res=   Instruction.isValidInstruction(instruction)
        then:
        !res
    }

    def "IsValidInstruction when instructions is TurnAround"() {
        given:
        def instruction=new Instruction()
        instruction.setRoboInstruction(Instruction.RoboInstruction.TURNAROUND)
        instruction.setFirstInstruction(1)
        when:
        def res=   Instruction.isValidInstruction(instruction)
        then:
        !res
    }

}
