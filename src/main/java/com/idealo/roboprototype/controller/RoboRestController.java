package com.idealo.roboprototype.controller;

import static com.idealo.roboprototype.controller.HomeController.COOKIE_TOKEN;

import com.idealo.roboprototype.dto.ErrorDto;
import com.idealo.roboprototype.model.Instruction;
import com.idealo.roboprototype.model.RoboWalkGrid;
import com.idealo.roboprototype.service.RoboService;
import com.idealo.roboprototype.validation.GenericValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "Constroller for Script and robo move")
@RestController
@Slf4j
@RequestMapping("/v1/robo")
public class RoboRestController {

  private final RoboService roboService;

  @Autowired
  RoboRestController(RoboService roboService) {
    this.roboService = roboService;
  }

  @ApiOperation(value = "Performs the script")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created Successfully"),
      @ApiResponse(code = 400, message = "Bad Request"),
      @ApiResponse(code = 500, message = "Server Failure")
  })
  @PostMapping(value = "/perform-operation", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
  public ResponseEntity performScript(HttpServletRequest request, @RequestBody String script) {
    String userToke = null;
    for (Cookie cookie : request.getCookies()) {
      if (cookie.getName().equals(COOKIE_TOKEN)) {
        userToke = cookie.getValue();

      }
    }
    GenericValidator.throwIfStringIsNullOrEmpty(script, new ErrorDto("Script is empty"));
    List<Instruction> instructions = roboService.readInstructions(script);
    RoboWalkGrid roboWalkGrid = roboService
        .performWalkForTheTokenAndInstructions(userToke, instructions);
    return new ResponseEntity(roboWalkGrid, HttpStatus.CREATED);
  }
}
