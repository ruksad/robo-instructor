package com.idealo.roboprototype.controller;

import com.idealo.roboprototype.model.RoboStore;
import com.idealo.roboprototype.model.RoboWalkGrid;
import java.util.UUID;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class HomeController {

  private final RoboStore roboStore;
  public static final String COOKIE_TOKEN = "userToken";

  @Autowired
  HomeController(RoboStore roboStore) {
    this.roboStore = roboStore;
  }

  @GetMapping(value = {"/"})
  public String homeEndpoint(HttpServletResponse response) {
    log.info("Inside home controller");
    String token = generateUUID();
    response.addCookie(new Cookie(COOKIE_TOKEN, token));
    roboStore.getStore().put(token, new RoboWalkGrid());
    return "index";
  }

  private static String generateUUID() {
    return UUID.randomUUID().toString().replace("-", "");
  }
}
