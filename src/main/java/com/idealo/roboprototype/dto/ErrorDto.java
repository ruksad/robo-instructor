package com.idealo.roboprototype.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ErrorDto {

  public static final Object[] EMPTY_MESSAGE_PARAMS = {};
  private String errorType;
  private String errorMessage;
  @JsonIgnore
  private String errorMessageKey;
  @JsonIgnore
  private Object[] errorMessageParams = EMPTY_MESSAGE_PARAMS;

  public ErrorDto(String errorType, String errorMessage, String errorMessageKey) {
    this(errorType, errorMessage, errorMessageKey, EMPTY_MESSAGE_PARAMS);
  }

  public ErrorDto(String errorType, String errorMessage) {
    this(errorType, errorMessage, "", EMPTY_MESSAGE_PARAMS);
  }

  public ErrorDto(String errorMessage) {
    this.errorMessage = errorMessage;
  }

}

