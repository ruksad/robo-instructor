package com.idealo.roboprototype.model;

import java.util.concurrent.ConcurrentHashMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class RoboStore {

  private ConcurrentHashMap<String, RoboWalkGrid> store;
}
