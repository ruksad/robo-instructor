package com.idealo.roboprototype.model;

import com.idealo.roboprototype.dto.ErrorDto;
import com.idealo.roboprototype.excpetions.GenericException;
import com.idealo.roboprototype.model.Instruction.RoboInstruction;
import com.idealo.roboprototype.model.Robo.Direction;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@EqualsAndHashCode
@ToString
public class RoboWalkGrid implements AbstractWalk {

  Robo currentRoboLocation=new Robo(1,1, Direction.EAST, RoboInstruction.WAIT.getValue());
  List<Robo> roboMoves = new ArrayList<>(10);
  List<ErrorDto> listOfInvalidInstructions = new ArrayList<>(10);

  @Override
  public void walk(List<Instruction> roboInstructions) {

    for (int i = 0; i < roboInstructions.size(); i++) {
      Robo robo = null;
      if (Instruction.isValidInstruction(roboInstructions.get(i))) {
        try {
          robo = Robo.moveRoboOnInstruction(currentRoboLocation, roboInstructions.get(i));
          robo.setInstruction(roboInstructions.get(i).getRoboInstruction().getValue());
        }catch (GenericException e){
          listOfInvalidInstructions.add(new ErrorDto(e.getMessage(),
              " For Invalid instruction at line " + (i+1) + " move will be ignoredBy robo"));
          log.info("Found invalid instructions {} ",roboInstructions.get(i));
        }

      } else {
        listOfInvalidInstructions.add(new ErrorDto("Invalid instruction at line " + (i+1)));
        log.info("instructions could not be parsed invalid instructions {} ",roboInstructions.get(i));
      }
      if(Objects.nonNull(robo)){
        final Robo robo1 = new Robo(robo.getHorizontalPosition(), robo.getVerticalPosition(),
            robo.getDirection(), robo.getInstruction());
        roboMoves.add(robo1);
      }

    }
  }


}
