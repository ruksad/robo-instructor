package com.idealo.roboprototype.model;

import com.idealo.roboprototype.excpetions.GenericException;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Robo {

  private int horizontalPosition;
  private int verticalPosition;
  private Direction direction;
  private String instruction;
  private static int[] nonMovable = {1, 2, 3, 4, 5};
  public static final int GRID_MAX_BOUND = 5;
  public static final int GRID_MIN_BOUND = 1;


  @Getter
  public enum Direction {
    EAST("east"), WEST("west"), SOUTH("south"), NORTH("north");
    private String value;

    Direction(String value) {
      this.value = value;
    }

    public static boolean checkValueExist(String value) {
      boolean flag = false;
      for (Direction status : values()) {
        if (status.value.equalsIgnoreCase(value)) {
          flag = true;
        }
      }
      return flag;
    }
  }

  public static Robo moveRoboOnInstruction(Robo currentRobo,
      Instruction roboInstruction) {
    Robo robo = null;
    switch (roboInstruction.getRoboInstruction()) {
      case WAIT:
        robo = currentRobo;
        break;
      case LEFT:
        robo = setLeftForRobo(currentRobo);
        break;
      case RIGHT:
        robo = setRightForRobo(currentRobo);
        break;
      case FORWARD:
        if (checkIfRoboCanMoveForwardOnCornerCases(currentRobo)) {

          robo = moveRoboForward(currentRobo, roboInstruction);

        } else {
          throw new GenericException("Robo cannot move forward going out of Grid");
        }
        break;
      case POSITION:
        robo = setPosition(currentRobo, roboInstruction);
        break;
      case TURNAROUND:
        robo = turnRoboAround(currentRobo);
    }
    return robo;
  }

  private static Robo setLeftForRobo(Robo currentRobo) {
    switch (currentRobo.getDirection()) {
      case EAST:
        currentRobo.setDirection(Direction.NORTH);
        break;
      case SOUTH:
        currentRobo.setDirection(Direction.EAST);
        break;
      case NORTH:
        currentRobo.setDirection(Direction.WEST);
        break;
      case WEST:
        currentRobo.setDirection(Direction.SOUTH);
        break;
      default:
        throw new GenericException("UnKnown direction of the ROBO");
    }
    return currentRobo;
  }

  private static Robo setRightForRobo(Robo currentRobo) {
    switch (currentRobo.getDirection()) {
      case EAST:
        currentRobo.setDirection(Direction.SOUTH);
        break;
      case SOUTH:
        currentRobo.setDirection(Direction.WEST);
        break;
      case NORTH:
        currentRobo.setDirection(Direction.EAST);
        break;
      case WEST:
        currentRobo.setDirection(Direction.NORTH);
        break;
      default:
        throw new GenericException("UnKnown direction of the ROBO");
    }
    return currentRobo;
  }

  private static Robo setPosition(Robo currentRobo, Instruction roboInstruction) {
    if (roboInstruction.getFirstInstruction() >= GRID_MIN_BOUND
        && roboInstruction.getFirstInstruction() <= GRID_MAX_BOUND
        && roboInstruction.getSecondInstruction() <= GRID_MAX_BOUND
        && roboInstruction.getSecondInstruction() >= GRID_MIN_BOUND) {
      currentRobo.setDirection(roboInstruction.getDirection());
      currentRobo.setHorizontalPosition(roboInstruction.getFirstInstruction());
      currentRobo.setVerticalPosition(roboInstruction.getSecondInstruction());
    } else {
      throw new GenericException("This position is not suitable for current grid");
    }
    return currentRobo;
  }

  private static Robo turnRoboAround(Robo currentRobo) {
    switch (currentRobo.getDirection()) {
      case EAST:
        currentRobo.setDirection(Direction.WEST);
        break;
      case WEST:
        currentRobo.setDirection(Direction.EAST);
        break;
      case NORTH:
        currentRobo.setDirection(Direction.SOUTH);
        break;
      case SOUTH:
        currentRobo.setDirection(Direction.NORTH);
        break;
        default:
    }
    return currentRobo;
  }

  private static Robo moveRoboForward(Robo currentRoboLocation, Instruction roboInstruction) {
    switch (currentRoboLocation.getDirection()) {
      case SOUTH:
        if ((roboInstruction.getFirstInstruction() + currentRoboLocation.getVerticalPosition())
            <= GRID_MAX_BOUND) {
          currentRoboLocation.setVerticalPosition(
              roboInstruction.getFirstInstruction() + currentRoboLocation.getVerticalPosition());
        } else {
          throw new GenericException(GenericException.ROBO_CANT_MOVE_FORWARD);
        }
        break;
      case NORTH:
        if ((currentRoboLocation.getVerticalPosition() - roboInstruction.getFirstInstruction())
            >= GRID_MIN_BOUND) {
          currentRoboLocation.setVerticalPosition(currentRoboLocation.getVerticalPosition() -
              roboInstruction.getFirstInstruction());
        } else {
          throw new GenericException(GenericException.ROBO_CANT_MOVE_FORWARD);
        }
        break;
      case WEST:
        if ((currentRoboLocation.getHorizontalPosition() - roboInstruction.getFirstInstruction())
            >= GRID_MIN_BOUND) {
          currentRoboLocation.setHorizontalPosition(currentRoboLocation.getHorizontalPosition() -
              roboInstruction.getFirstInstruction());
        } else {
          throw new GenericException(GenericException.ROBO_CANT_MOVE_FORWARD);
        }
        break;
      case EAST:
        if ((currentRoboLocation.getHorizontalPosition() + roboInstruction.getFirstInstruction())
            <= GRID_MAX_BOUND) {
          currentRoboLocation.setHorizontalPosition(currentRoboLocation.getHorizontalPosition() +
              roboInstruction.getFirstInstruction());
        } else {
          throw new GenericException(GenericException.ROBO_CANT_MOVE_FORWARD);
        }
        break;
      default:
    }
    return currentRoboLocation;
  }

  private static boolean checkIfRoboCanMoveForwardOnCornerCases(Robo currentRoboLocation) {
    boolean canMove = true;
    switch (currentRoboLocation.getDirection()) {
      case EAST:
        if ((currentRoboLocation.getVerticalPosition() == GRID_MAX_BOUND && Arrays.asList(nonMovable)
            .contains(currentRoboLocation.getHorizontalPosition()))) {
          canMove = false;
        }
        break;
      case WEST:
        if ((currentRoboLocation.getVerticalPosition() == GRID_MIN_BOUND && Arrays.asList(nonMovable)
            .contains(currentRoboLocation.getHorizontalPosition()))) {
          canMove = false;
        }
        break;
      case NORTH:
        if ((currentRoboLocation.getHorizontalPosition() == GRID_MIN_BOUND && Arrays
            .asList(nonMovable)
            .contains(currentRoboLocation.getVerticalPosition()))) {
          canMove = false;
        }
        break;
      case SOUTH:
        if ((currentRoboLocation.getHorizontalPosition() == GRID_MAX_BOUND && Arrays
            .asList(nonMovable)
            .contains(currentRoboLocation.getVerticalPosition()))) {
          canMove = false;
        }
        break;
        default:
    }
    return canMove;
  }

}
