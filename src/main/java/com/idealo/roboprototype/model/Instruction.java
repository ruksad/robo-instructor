package com.idealo.roboprototype.model;

import com.idealo.roboprototype.excpetions.GenericException;
import com.idealo.roboprototype.model.Robo.Direction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@EqualsAndHashCode
@Slf4j
public class Instruction {

  private RoboInstruction roboInstruction;
  private Integer firstInstruction=null;
  private Integer secondInstruction=null;
  private Direction direction;


  @Getter
  public enum RoboInstruction {
    POSITION("position"), FORWARD("forward"), WAIT("wait"), TURNAROUND("turnaround"), RIGHT(
        "right"), LEFT("left");
    private String value;

    RoboInstruction(String value) {
      this.value = value;
    }

    public static boolean checkIfValueExists(String value) {
      boolean flag = false;
      for (RoboInstruction roboInstruction : values()) {
        if (roboInstruction.value.equalsIgnoreCase(value)) {
          return true;
        }
      }
      return flag;
    }
  }

  public static boolean isValidInstruction(Instruction instruction) {
    boolean flag = true;

    switch (instruction.getRoboInstruction()) {
      case POSITION:
        if (Objects.isNull(instruction.getFirstInstruction()) || Objects.isNull(
            instruction.getSecondInstruction()) || Objects.isNull(instruction.getDirection())) {
          flag = false;
        }
        break;
      case FORWARD:
        if (Objects.isNull(instruction.getFirstInstruction())) {
          flag = false;
        }
        break;
      case WAIT:
        flag = isInstructionValidForWaitAndRightAndTurnAround(instruction);
        break;
      case RIGHT:
        flag = isInstructionValidForWaitAndRightAndTurnAround(instruction);
        break;
      case TURNAROUND:
        flag = isInstructionValidForWaitAndRightAndTurnAround(instruction);
        break;
      case LEFT:
        flag = isInstructionValidForWaitAndRightAndTurnAround(instruction);
        break;
      default:
        flag = false;
    }

    return flag;
  }

  private static boolean isInstructionValidForWaitAndRightAndTurnAround(Instruction instruction) {
    return (Objects.isNull(instruction.getFirstInstruction()) && Objects
        .isNull(instruction.getSecondInstruction()) && Objects
        .isNull(instruction.getDirection()));
  }

  public static List<Instruction> from(String string) {
    List<Instruction> instructions = new ArrayList<>(10);
    List<String> lines = Arrays.asList(string.split("\\r?\\n"));
    for (int i = 0; i < lines.size(); i++) {

      String[] split = lines.get(i).split("\\s+");
      if (split.length > 4 || split.length == 3) {
        throw new GenericException(
            "Command length is greater than 4 or command length is 3 at line " + i);
      }

      Instruction instruction = getInstruction(i, split);
      instructions.add(instruction);
    }
    return instructions;
  }

  private static Instruction getInstruction(int i, String[] split) {
    Instruction instruction = new Instruction();
    if (split.length == 4) {
      if (Direction.checkValueExist(split[3])) {
        instruction.setDirection(Direction.valueOf(split[3].toUpperCase()));
      } else {
        throw new GenericException("This Direction at line " + (i+1) + " does not exist");
      }
      try {
        instruction.setFirstInstruction(Integer.valueOf(split[1]));
        instruction.setSecondInstruction(Integer.valueOf(split[2]));
      } catch (NumberFormatException e) {
        throw new GenericException("Please check input values are wrong at line " + (i+1));
      }
    } else if (split.length == 2) {
      try {
        instruction.setFirstInstruction(Integer.valueOf(split[1]));
      } catch (NumberFormatException e) {
        throw new GenericException("Please check input values are wrong at line " + (i+1));
      }
    }

    if (RoboInstruction.checkIfValueExists(split[0])) {
      instruction.setRoboInstruction(RoboInstruction.valueOf(split[0].toUpperCase()));
    } else {
      throw new GenericException("This instruction at line " + (i+1) + " does not exist");
    }

    return instruction;
  }
}
