package com.idealo.roboprototype.model;

import java.util.List;

public interface AbstractWalk {
  void walk(List<Instruction> roboInstructions);
}
