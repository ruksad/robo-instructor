package com.idealo.roboprototype.service;

import com.idealo.roboprototype.model.Instruction;
import com.idealo.roboprototype.model.RoboWalkGrid;
import java.util.List;

public interface RoboService {

  List<Instruction> readInstructions(String instructions);

  RoboWalkGrid performWalkForTheTokenAndInstructions(String token, List<Instruction> instructions);
}
