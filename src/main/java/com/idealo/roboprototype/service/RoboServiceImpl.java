package com.idealo.roboprototype.service;

import com.idealo.roboprototype.model.Instruction;
import com.idealo.roboprototype.model.RoboStore;
import com.idealo.roboprototype.model.RoboWalkGrid;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RoboServiceImpl implements RoboService {

  private final RoboStore roboStore;

  @Autowired
  RoboServiceImpl(RoboStore roboStore) {
    this.roboStore = roboStore;
  }

  @Override
  public List<Instruction> readInstructions(String instructions) {
    return Instruction.from(instructions);
  }

  @Override
  public RoboWalkGrid performWalkForTheTokenAndInstructions(String token,
      List<Instruction> instructions) {
    log.info("Instructions recieved {}", instructions);
    RoboWalkGrid roboWalkGrid = this.roboStore.getStore().get(token);
    roboWalkGrid.getRoboMoves().retainAll(Collections.emptyList());
    roboWalkGrid.getListOfInvalidInstructions().retainAll(Collections.emptyList());
    roboWalkGrid.walk(instructions);
    log.info("ROBO moves after instructions {}",roboWalkGrid);
    return roboWalkGrid;
  }
}
