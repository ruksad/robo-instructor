package com.idealo.roboprototype;

import com.idealo.roboprototype.model.RoboStore;
import com.idealo.roboprototype.model.RoboWalkGrid;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoboInstructorApplication {

  @Autowired
  RoboStore roboStore;

  public static void main(String[] args) {
    SpringApplication.run(RoboInstructorApplication.class, args);
  }

  @PostConstruct
  public void createRoboStore() {
    ConcurrentHashMap<String, RoboWalkGrid> store = new ConcurrentHashMap<>(4);
    roboStore.setStore(store);
  }
}
