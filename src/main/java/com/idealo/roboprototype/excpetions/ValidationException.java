package com.idealo.roboprototype.excpetions;


import com.idealo.roboprototype.dto.ErrorDto;
import java.util.Collection;
import java.util.Collections;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@ToString
public class ValidationException extends RestAPIException {

  private final Collection<ErrorDto> errors;

  public ValidationException(HttpStatus errorCode, ErrorDto error) {
    super(errorCode, error.getErrorMessage());
    this.errors = Collections.singleton(error);
  }

}
