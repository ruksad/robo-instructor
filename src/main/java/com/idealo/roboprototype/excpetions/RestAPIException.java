package com.idealo.roboprototype.excpetions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;


@Getter
@Setter
public class RestAPIException extends RuntimeException {

  private final HttpStatus errorCode;

  public RestAPIException(HttpStatus errorCode, String message) {
    super(message);
    this.errorCode = errorCode;
  }

  public RestAPIException(String message) {
    super(message);
    this.errorCode = HttpStatus.INTERNAL_SERVER_ERROR;
  }
}
