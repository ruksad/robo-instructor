package com.idealo.roboprototype.excpetions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class GenericException extends RuntimeException {
  public static final String ROBO_CANT_MOVE_FORWARD="Robo cant move this forward";

  private final HttpStatus errorCode;

  public GenericException(String message) {
    super(message);
    this.errorCode = HttpStatus.INTERNAL_SERVER_ERROR;
  }
}
