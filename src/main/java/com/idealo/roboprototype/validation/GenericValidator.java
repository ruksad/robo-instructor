package com.idealo.roboprototype.validation;

import com.idealo.roboprototype.dto.ErrorDto;
import com.idealo.roboprototype.excpetions.ValidationException;
import java.util.Objects;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

@Slf4j
public class GenericValidator {

  static Predicate<Object> isNull = Objects::isNull;
  static Predicate<String> isEmpty = StringUtils::isEmpty;

  private  GenericValidator(){}
  public static void throwIfStringIsNullOrEmpty(String value, ErrorDto errorDto) {
    if (isEmpty.test(value)) {
      throwValidationException(errorDto);
    }
  }

  public static void checkNotNull(Object value, ErrorDto errorDto) {
    if (isNull.test(value)) {
      throwValidationException(errorDto);
    }
  }

  private static void throwValidationException(ErrorDto errorDto) {
    throw new ValidationException(HttpStatus.BAD_REQUEST, errorDto);
  }
}
