# Robo-instructor

This prototype has taken 24 working hours for me because
1. I have to make the images and move them accordingly which is out of my comport zone
2. some of the classes are TDD

To create this prototype we can you use two approaches
    1. create 5*5 array of ROBO class and utilise arrayIndexOutOfBound exception to make robo not move out the
       box
    2. Use nested conditions and make robo move

I have used second approach to create the prototype because
    1. Prototype is multi-player i.e more than one UI can be opened and instructions of one robo will
        not intervene with other robo i.e user concurrent hash map.
    2. Due to many Clients can play in one attempt so objects are hold in memory , and due to that second approach
        consumes less memory but with more instructions.


ProtoType is created in
1. Spring-boot
2. Angular 7

If you have following
1. java
2. mvn

then run command: mvn clean install
this command will build angular project and put it inside spring resources

if you do not have above setup the simply run this command: java -jar ./robo-instructor-0.0.1-SNAPSHOT.jar
 inside current directory of .jar file

run the jar and type in browser http://localhost:8080/

Sample input for the commands
1.
        POSITION 1 3 EAST
        FORWARD 3
        WAIT
        TURNAROUND
        FORWARD 1
        RIGHT
        FORWARD 2

2.     FORWARD 4
       RIGHT
       FORWARD 4


Remember:
   1. to make move car on the grid user will have to  give POSITION command if command for POSITION is not
      given then the initial position of the car will be
      vertical:1
      horizontal: 1
      direction: EAST
   2. Commands are case insensitive

   3.Commands sould be valid and one of these
      POSITION, FORWARD ,WAIT, TURNAROUND, RIGHT, LEFT

   4. if FORWARD commmand has invalid steps then the command will be ingored by robo and next
      command will be executed

   5. ROBO maintains the last move if client is not refreshed


EnhanceMents
1. more test cases
2. more user friendly error messages
3. displaying the initial position of the car when no position command is given
3. Should have used Controller advice for smooth handling of Exceptions