import { TestBed } from '@angular/core/testing';

import { RoboServiceService } from './robo-service.service';

describe('RoboServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoboServiceService = TestBed.get(RoboServiceService);
    expect(service).toBeTruthy();
  });
});
