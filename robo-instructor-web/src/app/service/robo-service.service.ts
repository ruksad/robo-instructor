import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoboServiceService {
  public static readonly BASE_URI: string = "http://localhost:8080";
  public static readonly SCRIPT_SAVE_URL: string = "/v1/robo//perform-operation";
  constructor(private http: HttpClient) { }

  submitScript(script: string): Observable<any> {
    return this.http.post(RoboServiceService.BASE_URI + RoboServiceService.SCRIPT_SAVE_URL, script);
  }
}
