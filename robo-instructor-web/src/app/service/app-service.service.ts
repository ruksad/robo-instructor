import { GameServiceService } from './game-service.service';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  isImageLoaded: EventEmitter<number> = new EventEmitter();
  constructor(private gameServiceService:GameServiceService) { }

  public createPlayGrid(canvasElement): void {
    this.gameServiceService.loadAssets(canvasElement).then((image) => {
      setTimeout(() => {
        this.isImageLoaded.emit();
      }, 1000);
    });
  }

  getImageLoadEmitter() : EventEmitter<number> {
    return this.isImageLoaded;
  }
}
