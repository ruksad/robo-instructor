import { RoboPosition } from './../config/robo-position';
import { Injectable, Input } from '@angular/core';
import * as CONFIG from './../config/config';


@Injectable({
  providedIn: 'root'
})
export class GameServiceService {

  @Input() public width: any = CONFIG.playGroundWidth;
  @Input() public height: any = CONFIG.playGroundHeight;


  instructions:any;
  context: CanvasRenderingContext2D;
  imageNorth: HTMLImageElement = null;
  imageSouth: HTMLImageElement = null;
  imageEast: HTMLImageElement = null;
  imageWest: HTMLImageElement = null;
  gameLoop =  null;

  moveNorth = false;
  moveSouth = false;
  moveWest = false;
  moveEast = false;

  roboPosition: RoboPosition = {
    x: 0 ,
    y: 0 ,
  };

  constructor() { }


  loadAssets(canvasElement: HTMLCanvasElement): Promise<void> {
    this.context = canvasElement.getContext('2d');
    canvasElement.width = this.width;
    canvasElement.height = this.height;

    return new Promise((resolve, reject) => {
      this.imageNorth = new Image();
      this.imageNorth.src = CONFIG.palletteNorth;
      this.imageNorth.width = 58;
      this.imageNorth.height = 128;

      this.imageSouth = new Image();
      this.imageSouth.src = CONFIG.palletteSouth;
      this.imageSouth.width = 58;
      this.imageSouth.height = 128;


      this.imageEast = new Image();
      this.imageEast.src = CONFIG.palletteEast;
      this.imageEast.width = 128;
      this.imageEast.height = 58;

      this.imageWest = new Image();
      this.imageWest.src = CONFIG.palletteWest;
      this.imageWest.width = 128;
      this.imageWest.height = 58;

      this.imageWest.onload = () => {
        resolve();
      };
    });

  }


  startGameLoop() {
    let instructionIndex = 0;
    this.gameLoop = setInterval(() => {
      this.cleanGround();
      if (instructionIndex === (this.instructions.roboMoves.length - 1)) {
        this.killGameLoop();
      }
      let isInstructionMovable=false;

      isInstructionMovable= this.chekIfInstructionsIsMovable(this.instructions.roboMoves[instructionIndex].instruction);
      this.handlePositionInstruction(this.instructions.roboMoves[instructionIndex]);

      this.readInstructions(this.instructions.roboMoves[instructionIndex], isInstructionMovable);
      instructionIndex++;
    }, 2000);
  }

  killGameLoop(){
    clearInterval(this.gameLoop);
  }
  cleanGround(): void {
    this.context.clearRect(0, 0, CONFIG.playGroundWidth, CONFIG.playGroundHeight);
  }


  readInstructions(instruction: any, insInstructionMovalbe: boolean) {
    switch (instruction.direction) {
      case 'NORTH':
        if (insInstructionMovalbe) {
          this.roboPosition.y = ((250 / 5) * (instruction.verticalPosition)) - 50;
        }
        this.createRobo(this.imageNorth, CONFIG.roboNorth, this.roboPosition);
        break;
      case 'SOUTH':
        if (insInstructionMovalbe) {
          this.roboPosition.y = ((250 / 5) * (instruction.verticalPosition)) - 50;
        }
        this.createRobo(this.imageSouth, CONFIG.roboSouth, this.roboPosition);
        break;
      case 'EAST':
        if (insInstructionMovalbe) {
          this.roboPosition.x = ((174 / 5) * (instruction.horizontalPosition)) - 35;
        }
        this.createRobo(this.imageEast, CONFIG.roboEast, this.roboPosition);
        break;
      case 'WEST':
        if (insInstructionMovalbe) {
          this.roboPosition.x = ((174 / 5) * (instruction.horizontalPosition)) - 35;
        }
        this.createRobo(this.imageWest, CONFIG.roboWest, this.roboPosition);
    }
  }

  createRobo(image: HTMLImageElement, roboConfig: any, roboPostion: RoboPosition) {
    this.context.drawImage(
      image,
      roboConfig.sX, roboConfig.sY,
      roboConfig.sWidth, roboConfig.sHeight,
      roboPostion.x, roboPostion.y,
      roboConfig.width, roboConfig.height
    );
  }

  public setRoboInstructions(instructions: any) {
    this.instructions = instructions;
  }

  public chekIfInstructionsIsMovable(instruction): boolean {
    return (instruction === 'forward') ? true : false;
  }

  handlePositionInstruction(instruction) {
    if (instruction.instruction === 'position') {
      this.createRoboOnPosition(instruction);
    }
  }

  createRoboOnPosition(instruction) {
    this.roboPosition.x = 0;
    this.roboPosition.y = 0;
    this.roboPosition.x += ((174 / 5) * (instruction.horizontalPosition)) - 35;
    this.roboPosition.y += ((250 / 5) * (instruction.verticalPosition)) - 30;
  }

  checkIfInstructionshasPostionInstruction() {
    const instructionsHasPostionInstructions = this.instructions.roboMoves.filter(instruction => instruction.instruction === 'position');
    return instructionsHasPostionInstructions.length > 0;
  }
}
