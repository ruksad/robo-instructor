import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoboPlaygroundComponent } from './robo-playground.component';

describe('RoboPlaygroundComponent', () => {
  let component: RoboPlaygroundComponent;
  let fixture: ComponentFixture<RoboPlaygroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoboPlaygroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoboPlaygroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
