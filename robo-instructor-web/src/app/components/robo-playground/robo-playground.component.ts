import { GameServiceService } from './../../service/game-service.service';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AppServiceService } from 'src/app/service/app-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-robo-playground',
  templateUrl: './robo-playground.component.html',
  styleUrls: ['./robo-playground.component.css']
})
export class RoboPlaygroundComponent implements AfterViewInit {

  roboInstructions: any;
  subscription: any;

  @ViewChild('canvas') public canvas: ElementRef;
  areGemeAssetsLoaded: boolean;

  constructor(private gameServiceService: GameServiceService, private appServiceService: AppServiceService,private toastr: ToastrService) {}

  ngAfterViewInit(): void {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.appServiceService.createPlayGrid(canvasEl);
  }

  @Input() set recievedRoboInstructions(value) {
    console.log('Instructions recieved', value);
    if (value && value.listOfInvalidInstructions.length > 0) {
      value.listOfInvalidInstructions.forEach(instruction => {
        this.toastr.error(instruction.errorMessage, instruction.errorType,{timeOut:10000});
      });
    }
    if (value && value.roboMoves.length > 0) {
      this.gameServiceService.setRoboInstructions(value);
      this.gameServiceService.startGameLoop();
    }
  }
}
