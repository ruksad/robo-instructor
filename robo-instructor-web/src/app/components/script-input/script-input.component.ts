import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-script-input',
  templateUrl: './script-input.component.html',
  styleUrls: ['./script-input.component.css']
})
export class ScriptInputComponent implements OnInit {

  userInput: string;

  @Output()
  userInputEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  scriptChaged(){
    this.userInputEmitter.emit(this.userInput);
  }
}
