
export const playGroundHeight = window.innerHeight;
export const playGroundWidth = 500;
export const palletteNorth = '../assets/img/sprites_north.png';
export const palletteSouth = '../assets/img/sprites_south.png';
export const palletteEast = '../assets/img/sprites_east.png';
export const palletteWest = '../assets/img/sprites_west.png';

export const roboSpeed = 5;

export const initialRoboPOsition = {
  horizontalPosition: 1,
  verticalPosition: 1,
  direction: 'EAST'
}

export const roboNorth = {
  sX: 410,  // Black Viper car
  sY: 265,
  sWidth: 64,
  sHeight: 135,
  width: 20,
  height: 45,
};

export const roboSouth = {
  sX: 220,  // Black Viper car
  sY: 8,
  sWidth: 64,
  sHeight: 135,
  width: 20,
  height: 45,
};

export const roboEast = {
  sX: 8,  // Black Viper car
  sY: 414,
  sWidth: 150,
  sHeight: 65,
  width: 45,
  height: 20,
};
export const roboWest = {
  sX: 266,  // Black Viper car
  sY: 222,
  sWidth: 150,
  sHeight: 65,
  width: 45,
  height: 20,
};

export interface roboMove {
  x: number;
  y: number;
  width: number;
  height: number;
  update: Function;
}
