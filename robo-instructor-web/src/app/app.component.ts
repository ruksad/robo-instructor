import { GameServiceService } from './service/game-service.service';
import { RoboServiceService } from './service/robo-service.service';
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'robo-instructor-web';
  roboInstructions: any;
  constructor(private roboServiceService: RoboServiceService, private gameServiceService: GameServiceService
    ,private toastr: ToastrService) {}

  onUserInputRecieved($event) {
      if ($event) {
        this.roboServiceService.submitScript($event).subscribe(res => {
          this.roboInstructions = res;
        }, (error) => {
          this.toastr.error(error.error.message);
        }, () => {
          this.toastr.success("Script submitted");
        });
      }
  }
}
